import React from "react";
import { Component } from "./components/Component";
import ComponentNested from "nested/ComponentNested";

export const App = () => {
  console.log("hola");

  return (
    <div>
      <ComponentNested />
      <Component />
      hola nuevamente
    </div>
  );
};
