import path from "path";
import webpack from "webpack";
import HtmlWebpackPlugin from "html-webpack-plugin";
const ROOT_DIR = path.resolve(__dirname);
const SRC_DIR = `${ROOT_DIR}/src`;

const configWebpack = (env: any, arg: any): webpack.Configuration => {
  const { mode } = arg;
  const productionMode = mode === "production";
  return {
    devtool: !productionMode && "source-map",
    entry: `${ROOT_DIR}/index.tsx`,
    output: {
      path: `${ROOT_DIR}/build`,
      filename: productionMode ? "[name].[contenthash].js" : "bundle.js",
      clean: true,
    },
    resolve: {
      alias: {
        nested: `${SRC_DIR}/components/nested1`,
      },
      extensions: [".ts", ".tsx", ".js"],
    },
    module: {
      rules: [
        {
          test: [/\.tsx$/, /\.jsx$/, /\.ts$/, /\.js$/],
          exclude: "/node_modules/",

          use: {
            loader: "babel-loader",
            options: {
              presets: [
                "@babel/preset-env",
                "@babel/preset-react",
                "@babel/preset-typescript",
              ],
            },
          },
        },
        {
          test: [/\.html$/],
          use: "html-loader",
          exclude: "/node_modules/",
        },
        {
          test: [/\.s[ac]ss$/i],
          use: [
            { loader: "style-loader" }, // to inject the result into the DOM as a style block
            { loader: "css-modules-typescript-loader" }, // to generate a .d.ts module next to the .scss file (also requires a declaration.d.ts with "declare modules '*.scss';" in it to tell TypeScript that "import styles from './styles.scss';" means to load the module "./styles.scss.d.td")
            { loader: "css-loader", options: { modules: true } }, // to convert the resulting CSS to Javascript to be bundled (modules:true to rename CSS classes in output to cryptic identifiers, except if wrapped in a :global(...) pseudo class)
            { loader: "sass-loader" }, // to convert SASS to CSS
            // NOTE: The first build after adding/removing/renaming CSS classes fails, since the newly generated .d.ts typescript module is picked up only later
          ],
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        filename: "./index.html",
        template: `${ROOT_DIR}/public/index.html`,
        inject: true,
      }),
    ],
  };
};
export default configWebpack;
